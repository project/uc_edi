
-- SUMMARY --

The Ubercart EDI filesystem module provides a local filesystem delivery method
for the Ubercart EDI module.


-- REQUIREMENTS --

* Ubercart EDI: http://drupal.org/project/uc_edi


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Resources
  - Export Delivery Methods settings page:
    admin/store/settings/edi/export/delivery-methods
  - Order export files: admin/store/edi/orders/export/files
  - Ubercart EDI filesystem permissions:
    admin/user/permissions#module-uc_edi_filesystem

* Description
  The Ubercart EDI filesystem module provides a Filesystem Export Delivery
  Method. The user must specify directories on the local filesystem for saving
  and archiving export files to. Export files can be downloaded and archived
  from the Export Files page.

* Directories
  Set the "Export directory" and "Export archive directory" fields to
  directories on the local filesystem where export files will be written too,
  preferably outside the Drupal root folder.

* Permissions
  - download edi orders
    Users in roles with the "download edi orders" permissions can download and
    archive EDI order export files.


-- CONTACT --

Current maintainers:
* Jon Antoine (AntoineSolutions) - http://drupal.org/user/192192

This project has been sponsored by:
* Antoine Solutions
  Specializing in Drupal powered sites, Antoine Solutions offers Design,
  Development, Search Engine Optimisation (SEO) and Search Engine Marketing
  (SEM). Visit http://www.antoinesolutions.com for more information.

* Showers Pass
  Technically engineered cycling gear for racers, commuters, messengers and
  everyday cycling enthusiasts. Visit http://www.showerspass.com for more
  information.
