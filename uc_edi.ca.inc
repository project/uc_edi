<?php

/**
 * @file
 * Ubercart EDI Conditional Actions hooks and functions.
 */

/**
 * Implements hook_ca_trigger().
 */
function uc_edi_ca_trigger() {
  $triggers['uc_edi_order_export'] = array(
    '#title' => t('Order has been exported'),
    '#category' => t('EDI'),
    '#arguments' => array(
      'order' => array(
        '#entity' => 'uc_order',
        '#title' => t('Order'),
      ),
    ),
  );

  return $triggers;
}

/**
 * Implements hook_ca_predicate().
 */
function uc_edi_ca_predicate() {
  $predicates['uc_edi_order_export'] = array(
    '#title' => t('Export the order'),
    '#class' => 'edi',
    '#trigger' => 'uc_checkout_complete',
    '#status' => variable_get('uc_edi_order_export_method', 'cron') == 'checkout' ? 1 : 0,
    '#weight' => 5,
    '#actions' => array(
      array(
        '#name' => 'uc_edi_action_order_export',
        '#title' => t('Export the order.'),
        '#argument_map' => array(
          'order' => 'order',
        ),
      ),
    ),
  );

  $predicates['uc_edi_order_export_update'] = array(
    '#title' => t('Update an order after export'),
    '#class' => 'edi',
    '#trigger' => 'uc_edi_order_export',
    '#status' => 1,
    '#actions' => array(
      array(
        '#name' => 'uc_order_update_status',
        '#title' => t('Update the order status to Shipment pending.'),
        '#argument_map' => array(
          'order' => 'order',
        ),
        '#settings' => array(
          'order_status' => 'edi_pending',
        ),
      ),
      array(
        '#name' => 'uc_order_action_add_comment',
        '#title' => t('Add an order export comment.'),
        '#description' => t('Enter a message displayed on orders when an order gets exported.'),
        '#argument_map' => array(
          'order' => 'order',
        ),
        '#settings' => array(
          'comment_type' => 'order',
          'comment' => t('Order processed.'),
        ),
      ),
      array(
        '#name' => 'uc_order_action_add_comment',
        '#title' => t('Add an admin comment.'),
        '#argument_map' => array(
          'order' => 'order',
        ),
        '#settings' => array(
          'comment_type' => 'admin',
          'comment' => t('Order exported by EDI module.'),
        ),
      ),
    ),
  );

  return $predicates;
}

/**
 * Implements hook_action().
 */
function uc_edi_ca_action() {
  $order = array(
    '#entity' => 'uc_order',
    '#title' => t('Order'),
  );

  $actions['uc_edi_action_order_export'] = array(
    '#title' => t('Export an order'),
    '#category' => t('EDI'),
    '#callback' => 'uc_edi_action_order_export',
    '#arguments' => array(
      'order' => $order,
    ),
  );

  return $actions;
}

/******************************************************************************
 * Conditional Action Callbacks and Forms                                     *
 ******************************************************************************/

/**
 * Action for exporting an order.
 *
 * @param $order
 *   Ubercart order object.
 * @param $settings
 *   Default values for form elements.
 */
function uc_edi_action_order_export($order, $settings) {
  uc_edi_export_order($order);
}
