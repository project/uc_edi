<?php

/**
 * @file
 * This file is the default EDI export template.
 *
 * Available variables:
 * - $uc_edi_header: Is TRUE if this is the first order in the export (header
 *   should be printed if desired), otherwise FALSE.
 * - $uc_edi_footer: Is TRUE if this is the last order in the export (footer
 *   should be printed if desired), otherwise FALSE.
 * - $order: The Ubercart order object.
 * - $products: An array of product objects belonging to the order.
 * - $line_items: An array of line items belonging to the order.
 *
 * Variables added by Ubercart EDI via the Token module.
 * - $order_edi_batch_id: The export batch number.
 * - $order_payment_method: The order payment method.
 * - $order_shipping_charges: The "altered" order shipping charges.
 * - $order_shipping_charges_formatted: The "formatted" order shipping charges.
 * - $order-tax-charges: The "altered" order total tax charges.
 * - $order-tax-charges-formatted: The "formatted" order total tax charges.
 * - $order_shipping_first_name: The first name of the order shipping address.
 * - $order_shipping_last_name: The last name of the order shipping address.
 * - $order_shipping_company: The company of the order shipping address.
 * - $order_shipping_street1: The street1 of the order shipping address.
 * - $order_shipping_street2: The street2 of the order shipping address.
 * - $order_shipping_city: The city of the order shipping address.
 * - $order_shipping_zone: The zone/province/state of the order shipping address.
 * - $order_shipping_zone_code: The zone/province/state code of the order shipping address.
 * - $order_shipping_postal_code: The postal/zip code of the order shipping address.
 * - $order_shipping_country: The country ISO 3166-1 Numeric-3 code of the order shipping address.
 * - $order_shipping_country_name: The country ISO 3166-1 English short name of the order shipping address.
 * - $order_shipping_country_code_2: The country ISO 3166-1 Alpha-2 code of the order shipping address.
 * - $order_shipping_country_code_3: The country ISO 3166-1 Alpha-3 code of the order shipping address.
 * - $order_billing_first_name: The first name of the order billing address.
 * - $order_billing_last_name: The last name of the order billing address.
 * - $order_billing_company: The company of the order billing address.
 * - $order_billing_street1: The street1 of the order billing address.
 * - $order_billing_street2: The street2 of the order billing address.
 * - $order_billing_city: The city of the order billing address.
 * - $order_billing_zone: The zone/province/state of the order billing address.
 * - $order_billing_zone_code: The zone/province/state code of the order billing address.
 * - $order_billing_postal_code: The postal/zip code of the order billing address.
 * - $order_billing_country: The country ISO 3166-1 Numeric-3 code of the order billing address.
 * - $order_billing_country_name: The country ISO 3166-1 English short name of the order billing address.
 * - $order_billing_country_code_2: The country ISO 3166-1 Alpha-2 code of the order billing address.
 * - $order_billing_country_code_3: The country ISO 3166-1 Alpha-3 code of the order billing address.
 * - $order_date_created_unix: The Unix/POSIX time the order was created.
 * - $order_date_modified_unix: The Unix/POSIX time the order was last modified.
 *
 * @see template_preprocess_uc_edi()
 * @see uc_price()
 *
 * @ingroup themeable
 */

// Set the delimiter.
$delimiter = "\t";

// Set the headers.
$headers = array(
  'customer_account_number',
  'customer_name',
  'order_number',
  'order_date',
  'sku',
  'quantity',
  'price',
);

if ($uc_edi_header) {
  /* -- Delete this line to print a list of available variables and their values.
  echo check_plain(print_r($variables, 1));
  // */

  // Print the header.
  echo implode($delimiter, $headers) . "\n";
}

// Set the default price context.
$context = array(
  'revision' => 'altered',
  'type' => 'order_product',
);

// Loop through all products printing all order and product values.
for ($line = 0; $product = $products[$line]; ++$line) {
  // Set the product specific price context.
  $price_info = array(
    'price' => $product->price,
    'qty' => $product->qty,
  );
  $context['subject'] = array(
    'order_product' => $product,
  );

  // Print the values.
  $values = array(
    $order_uid,
    $order_billing_first_name . ' ' . $order_billing_last_name,
    $order_id,
    $order_date_created_unix,
    $product->model,
    $product->qty,
    uc_price($price_info, $context),
  );
  echo implode($delimiter, $values) . "\n";
}

// Re-print headers as footers.
if ($uc_edi_footer) {
  echo implode($delimiter, $headers) . "\n";
}
