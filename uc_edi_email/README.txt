
-- SUMMARY --

The Ubercart EDI e-mail module provides an e-mail delivery method for the
Ubercart EDI module.


-- REQUIREMENTS --

* Ubercart EDI: http://drupal.org/project/uc_edi
* Mime Mail: http://drupal.org/project/mimemail


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Resources
  - Export Delivery Methods settings page:
    admin/store/settings/edi/export/delivery-methods

* Description
  The Ubercart EDI e-mail module provides an E-mail Export Delivery Method. The
  export file will be attached to an e-mail using the specified settings.


-- CONTACT --

Current maintainers:
* Jon Antoine (AntoineSolutions) - http://drupal.org/user/192192

This project has been sponsored by:
* Antoine Solutions
  Specializing in Drupal powered sites, Antoine Solutions offers Design,
  Development, Search Engine Optimisation (SEO) and Search Engine Marketing
  (SEM). Visit http://www.antoinesolutions.com for more information.

* Showers Pass
  Technically engineered cycling gear for racers, commuters, messengers and
  everyday cycling enthusiasts. Visit http://www.showerspass.com for more
  information.
